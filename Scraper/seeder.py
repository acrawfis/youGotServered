#!/usr/bin/env python2.7

#######################################
# Student:  Aaron Crawfis
# netID:    acrawfis
# Date:     01 December 2016
# Class:    CSE 30246
# File:     seeder.py
# Purpose:  Seed DB w/ ND Data
#######################################

# Libraries -------------------------------------------------------------------
import sys
import time
import MySQLdb

# Global Variables ------------------------------------------------------------
ND_ADDR =       'http://ur.nd.edu/request/eds.php?uid={}&full_response=true'

SQL_ADDR =      'localhost'
SQL_USER =      'root'
SQL_PASSWD =    'Server$8888'
SQL_DB =        'yougotservered'

ID_FILE =       'list6.txt'

# Functions -------------------------------------------------------------------

# Main ------------------------------------------------------------------------

# Connect to Database
db = MySQLdb.connect(host = SQL_ADDR,
                     user = SQL_USER,
                     passwd = SQL_PASSWD,
                     db = SQL_DB)
cursor = db.cursor()

# Iterate Over all IDs
# Open netID Input File
idFile = open(ID_FILE, 'r')
ids = list()
for id in idFile:
    ids.append(id.rstrip())
idFile.close()

i = 0
for netID1 in ids:
    i=i+1
    for netID2 in ids[i:]:
        # Insert into Database
        try:
            sql = 'INSERT INTO connections (netID1, netID2, cTime) VALUES ("{}", "{}", "{}")'.format(netID1, netID2, time.strftime('%Y-%m-%d %H:%M:%S'))
            print sql
            cursor.execute(sql)
            db.commit()
        except:
            print "ERROR"
            db.rollback()

# Close Database
db.close()

# Close File
idFile.close()
