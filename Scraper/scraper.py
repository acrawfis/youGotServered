#!/usr/bin/env python2.7

#######################################
# Student:  Aaron Crawfis
# netID:    acrawfis
# Date:     25 September 2016
# Class:    CSE 30246
# File:     main.py
# Purpose:  Scrape ND Data and insert
#           into database
#######################################

# Libraries -------------------------------------------------------------------
import sys
import json
import urllib
import MySQLdb

# Global Variables ------------------------------------------------------------
ND_ADDR =       'http://ur.nd.edu/request/eds.php?uid={}&full_response=true'

SQL_ADDR =      'localhost'
SQL_USER =      'acrawfis'
SQL_PASSWD =    'cse30246'
SQL_DB =        'people'

ID_FILE =       'sortedNames.txt'
#ID_FILE =       'testFile.txt'

# Functions -------------------------------------------------------------------

# Main ------------------------------------------------------------------------

# Get Password
#SQL_PASSWD = raw_input("Database password: ")

# Open netID Input File
idFile = open(ID_FILE, 'r')

# Connect to Database
db = MySQLdb.connect(host = SQL_ADDR,
                     user = SQL_USER,
                     passwd = SQL_PASSWD,
                     db = 'acrawfis')
cursor = db.cursor()

# Clear database
cursor.execute('DROP TABLE IF EXISTS {}'.format(SQL_DB))

# Create Database
sql = """CREATE TABLE {} (
         netID CHAR(15) NOT NULL PRIMARY KEY,
         firstName CHAR(20),
         middleName CHAR(20),
         lastName CHAR(20),
         department VARCHAR(30),
         year INT(4))""".format(SQL_DB)
cursor.execute(sql)

# Iterate Over all IDs
for id in idFile:
    id = id.rstrip()
    
    # Query Database
    response = urllib.urlopen(ND_ADDR.format(id))
    try:
        data = json.loads(response.read())
    except:
        continue

    # Error Check
    if 'ndformalname' in data:
        firstName = 'NULL'
        middleName = 'NULL'
        lastName = 'NULL'
        names = data['ndformalname'].split(' ')
        firstName = names[0]
        if len(names) == 2: lastName = names[1]
        elif len(names) >= 3:
            middleName = names[1]
            lastName = names[2]
    else:
        continue
    
    # Print Results
    print data['ndformalname']
    
    # Extract Data


    if 'ndtoplevelprimarydepartment' in data:
        department = data['ndtoplevelprimarydepartment']
    else:
        continue

    if 'ndentrycreatedate' in data:
        year = int(data['ndentrycreatedate'][0:4]) + 4
    else:
        continue

    # Insert into Database
    try:
        sql = 'INSERT INTO {} (netID, firstName, middleName, lastName, department, year) VALUES ("{}", "{}", "{}", "{}", "{}", {})'.format(SQL_DB, id, firstName, middleName, lastName, department, year)
        #print sql
        cursor.execute(sql)
        db.commit()
    except:
        print "ERROR"
        db.rollback()

# Show results
cursor.execute('SELECT * FROM {}'.format(SQL_DB))

# Close Database
db.close()

# Close File
idFile.close()
