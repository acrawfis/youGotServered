##############################################################################
# File:     app.py
# Authors:  Aaron Crawfis, Ryan Hooley, Ryan Hesse, Zach Janicki
# Date:     1 November 2016
# Purpose:  Main flask handler and all app logic code
###############################################################################

# Libraries -------------------------------------------------------------------

## Flask and Flask Login
from flask import Flask, render_template, request, redirect, url_for
import flask_login

import sys                              # System Calls
import json                             # JSON Library for Data Reading
import urllib                           # Read Data from ND Website
import MySQLdb                          # Communicate with mySQL Database
import time                             # Generate time for connections
from  passlib.hash import sha256_crypt  # Password hashing for logins
from collections import Counter

# Global Variables ------------------------------------------------------------

## Login information for database
SQL_ADDR =      'localhost'
SQL_USER =      'root'
SQL_PASSWD =    'Server$8888'
SQL_DB =        'yougotservered'

## Flask_login information
app = Flask(__name__)
app.secret_key = 'You Got Servered!!'
login_manager = flask_login.LoginManager()
login_manager.init_app(app)

if __name__ == "__main__":
    app.run()

# Database Setup ---------------------------------------------------------------

# Create Database
db = MySQLdb.connect(host = SQL_ADDR,
                     user = SQL_USER,
                     passwd = SQL_PASSWD,
                     db = SQL_DB)

# Create Cursor
cursor = db.cursor()

# Functions ---------------------------------------------------------------

# Given a netID, return the infromation for that user, as long as user is not deactivated
# Return format: tuple[netID, firstName, middleName, lastName, department, phone, level, isDeactivated]
def searchForNetID(netID):
    # Format SQL
    sql = 'SELECT * FROM people WHERE netID = "{}" AND isDeactivated = 0'.format(netID)
    # Execute SQL
    cursor.execute(sql)
    # Collect Resutls
    rows = cursor.fetchall()
    # Return results
    if rows:
	    return rows[0]
    else:
	    return 0

# Opt out a user from the game
# Sets the isDeactivated flag in the people table for a netID to 1
def optOutUser(cursor, netID):
    # Format SQL
    sql = 'UPDATE people SET isDeactivated = 1 WHERE netID = "{}"'.format(netID)
    # Execute SQL
    cursor.execute(sql)
    db.commit()

# Create a connection between two netIDs
# Adds the current timestamp
# Prevent duplicate connections
def createConnection(netID1, netID2):
    # Format SQL
    sql = 'SELECT * FROM ( SELECT netID2, netID1, cTime FROM connections WHERE netID1 = "{}" AND netID2 = "{}" UNION ALL (SELECT * FROM connections WHERE netID1 = "{}" AND netID2 = "{}")) B;'.format(netID1, netID2, netID2, netID1)
    cursor.execute(sql)
    rows = cursor.fetchall()
    if len(rows) > 0:
        return False
    
    # Format SQL
    sql = 'INSERT INTO connections (netID1, netID2, cTime) VALUES ("{}", "{}", "{}")'.format(netID1, netID2, time.strftime('%Y-%m-%d %H:%M:%S'))
    # Execute SQL
    cursor.execute(sql)
    db.commit()
    return True

# Return all connections for a givem netID 
# Searches for both connections where user initiated and other user initiated
# Return format: list(netID (of other user), netID (you), timestamp, netID (of other user), firstName, middleName, lastName, department, phone, level, isDeactivated)
def searchForConnections(netID):
    # Format SQL
    sql = 'SELECT * FROM ( SELECT netID2, netID1, cTime FROM connections WHERE netID1 = "{}" UNION ALL (SELECT * FROM connections WHERE netID2 = "{}")) B ORDER BY cTime;'.format(netID, netID)
    # Execute SQL
    cursor.execute(sql)
    # Collect Resutls
    rows = cursor.fetchall()
    # Create new list
    connections = list()
    # Get names
    for row in rows:
        p0 = searchForNetID(row[0])
        if p0 == 0:
            continue
        connections.append(row + p0)
    # Return results
    return connections

# Return a list of every user and their ranking among all players
# Return type: list(rank, netID, numConnections)
def buildLeaderboard():
    sql = "SELECT L.netID, COUNT(L.netID) FROM ( SELECT C.netID1 as 'netID' FROM connections C UNION ALL ( SELECT D.netID2 as 'netID' FROM connections D ) ) L GROUP BY L.netID ORDER BY COUNT(L.netID) DESC"
    cursor.execute(sql)
    rows = cursor.fetchall()
    people = list()
    i = 0
    for row in rows:
        p0 = searchForNetID(row[0])
        # User doesn't exist
        if p0 == 0:
            continue
        sql = 'SELECT * FROM users WHERE netID="{}"'.format(p0[0])
        cursor.execute(sql)
        results = cursor.fetchall()
        if len(results) > 0:
            i = i+1
            numMissions = 0
            Missions = listCompletedMissions(p0[0])
            for k in Missions.keys():
                if Missions[k] == True:
                    numMissions += 1
            people.append([i, row[0], row[1], numMissions])
        else:
            continue
    return people

# Returns a set of nodes and edges for connection graph
# Return format: list(nodeID, netID, name, prevNode, checked)
def buildConGraph(netID):
    graph = list()
    x = searchForNetID(netID)
    graph.append([1, x[1] + ' ' + x[3] ,[], netID, True])
    theDict = {graph[0][1] : graph[0][0]}
    loop = 0
    c = graph[0]
    current = netID
    currentN = graph[0][1]
    currentID = 1
    newID = 2
    while loop == 0:
        conns = searchForConnections(current)
        for conn in conns:
            nope = 0
            for i in graph:
                if i[1] == conn[4] + ' ' + conn[6]:
                    nope = 1
                    break
            if nope == 0:
                c[2].append(conn[4] + ' ' + conn[6])
                graph.append([newID, conn[4] + ' ' + conn[6], [], conn[0], False])
                theDict[conn[4] + ' ' + conn[6]] = newID
                newID += 1
        loop = 1
        for i in graph:
            if i[4] == False:
                loop = 0
                c = i
                current = i[3]
                currentID = i[0]
                i[4] = True
                break
    for i in graph:
        for j in range(0,len(i[2])):
            i[2][j] = theDict[i[2][j]]
    return graph
        
# Return the rank of a User
# Return type: Either string 'N/A' or int rank
def getUserRank(netID):
    lb = buildLeaderboard()
    rank = 'N/A'
    for people in lb:
        if people[1] == netID:
            rank = people[0]
            break
    return rank

# Flask-Login ------------------------------------------------------------------

# Create a user for the first time given a netID and password
def createUser(password, netID):
    # Check for Duplicate
    sql = 'SELECT * FROM users where netID="{}"'.format(netID)
    cursor.execute(sql)
    rows = cursor.fetchall()
    if len(rows)>0:
        return False
    # Check for Valid netID:
    sql = 'SELECT * FROM people where netID="{}"'.format(netID)
    cursor.execute(sql)
    rows = cursor.fetchall()
    if len(rows) == 0:
        return False

    # Setup email
    email = netID + '@nd.edu'
    # Encrypt
    hashed_password = sha256_crypt.encrypt(password)
    # Format SQL
    sql = 'INSERT INTO users (email, passHash, netID) VALUES ("{}", "{}", "{}")'.format(email, hashed_password, netID)
    # Execute SQL
    cursor.execute(sql)
    db.commit()

    return True

# Checks if user exists
# Return type: Either true or false depending on if netID is in users table
def doesUserExist(cursor, netID):
    # Format SQL
    sql = 'SELECT count(*) from users WHERE netID = "{}"'.format(netID)
    # Execute SQL
    cursor.execute(sql)
    # Get Count
    count = cursor.fetchall()
    result = count[0][0]
    # Check if user is deactivated
    p0 = searchForNetID(netID)
    if p0 == 0:
        return False
    # Check if User Exists
    if result:
        return True
    else:
        return False

# Get the hash from the users table for a netID
# Return type: string
def getUserHash(cursor, netID):
    # Format SQL
    sql = 'SELECT passHash from users WHERE netID = "{}"'.format(netID)
    # Execute SQL
    cursor.execute(sql)
    # Get Count
    user = cursor.fetchall()
    # Return password hash
    return user[0][0]

# Check if login credentials are valid
# Return type: bool determined by validity of login credentials
def isLoginValid(cursor, netID, password):
    if not doesUserExist(cursor, netID):
        return False
    hash = getUserHash(cursor, netID)
    return sha256_crypt.verify(password, hash)

# Required function for flask-login.
class User(flask_login.UserMixin):
    pass

# User loader function for flask-login. Logs in user.
@login_manager.user_loader
def load_user(user_id):
    if not doesUserExist(cursor, user_id):
        return
    
    user = User()
    user.id = user_id
    return user

#Sugested friends triadic closure 
def findSuggestedFriends(netID):
    # Create new lists
    netIds2 = list()    # List of netIDs of 1st Degree Connections
    netIds3 = list()    # List of netIDs of 2nd Degree Connections  

    # Format SQL
    sql = 'SELECT * FROM ( SELECT netID2, netID1, cTime FROM connections WHERE netID1 = "{}" UNION ALL (SELECT * FROM connections WHERE netID2 = "{}")) B'.format(netID, netID)
    # Execute SQL
    cursor.execute(sql)
    # Collect Resutls
    rows = cursor.fetchall()

    # Track every 1st degree connection
    for row in rows:
        netIds2.append(row[0])

    for p1 in netIds2:
        # Track every second degree connection
        sql = 'SELECT * FROM ( SELECT netID2, netID1, cTime FROM connections WHERE netID1 = "{}" UNION ALL (SELECT * FROM connections WHERE netID2 = "{}")) B'.format(p1, p1)
        cursor.execute(sql)
        rows = cursor.fetchall()
        for row in rows:
            if row[0] != netID and row[0] not in netIds2:
                netIds3.append(row[0])

    # Return results
    return Counter(netIds3).most_common(15)

# Mission functions/queries ----------------------------------------------------

mission_list = ['Connect With 5 Engineers',
                'Connect With 5 Scientists',
                'Connect With 5 Arts and Letters Students',
                'Connect With 5 Mendoza Students',
                'Connect With 10 Freshman',
                'Connect With 10 Sophomores',
                'Connect With 10 Juniors',
                'Connect With 10 Seniors']

def listCompletedMissions(netID):
    completed_missions = dict()
    for mission in mission_list:
        if missionComplete(mission, netID):
            completed_missions[mission] = True
        else:
            completed_missions[mission] = False
    #print completed_missions
    return completed_missions

# checks to see if mission <mission> has been completed for person <netID>
# returns true or false
def missionComplete(mission, netID):
    major_index = 7
    class_index = 9
    # Major missions
    #print mission == 'Connect with 5 Engineers'
    if mission == 'Connect With 5 Engineers':
        # check for 5 engineering connections
        #print "mission is connect"
        all_connections = searchForConnections(netID)
        eng_connection_count = 0
        for connection in all_connections:
            if connection[major_index] == 'College of Engineering':
                eng_connection_count += 1
                #print connection[major_index]
        if eng_connection_count >= 5:
            return True
    if mission == 'Connect With 5 Scientists':
        # check for 5 science connections
        all_connections = searchForConnections(netID)
        sci_connection_count = 0
        for connection in all_connections:
            if connection[major_index] == 'College of Science':
                sci_connection_count += 1
        if sci_connection_count >= 5:
            return True
    if mission == 'Connect With 5 Arts and Letters Students':
        # check for 5 science connections
        all_connections = searchForConnections(netID)
        als_connection_count = 0
        for connection in all_connections:
            if connection[major_index] == 'College of Arts & Letters':
                als_connection_count += 1
        if als_connection_count >= 5:
            return True
    if mission == 'Connect With 5 Mendoza Students':
        # check for 5 science connections
        all_connections = searchForConnections(netID)
        bus_connection_count = 0
        for connection in all_connections:
            if connection[major_index] == 'Mendoza College of Business':
                bus_connection_count += 1
        if bus_connection_count >= 5:
            return True

    # Class missions
    #print mission
    if mission == 'Connect With 10 Freshman':
        all_connections = searchForConnections(netID)
        conn_count = 0
        for connection in all_connections:
            if connection[class_index] == 'Freshman':
                conn_count += 1
        if conn_count >= 5:
            return True
    if mission == 'Connect With 10 Sophomores':
        all_connections = searchForConnections(netID)
        conn_count = 0
        for connection in all_connections:
            if connection[class_index] == 'Sophomore':
                conn_count += 1
        if conn_count >= 5:
            return True
    if mission == 'Connect With 10 Juniors':
        all_connections = searchForConnections(netID)
        conn_count = 0
        for connection in all_connections:
            if connection[class_index] == 'Junior':
                conn_count += 1
        if conn_count >= 5:
            return True
    if mission == 'Connect With 10 Seniors':
        all_connections = searchForConnections(netID)
        conn_count = 0
        for connection in all_connections:
            if connection[class_index] == 'Senior':
                conn_count += 1
        if conn_count >= 5:
            return True
    return False

# Objects ----------------------------------------------------------------------

# Object to package all the data required for the dashboard.
class dashObj(object):

    def __init__(self):
        self.netID = ""
        self.name = ""

    def loadDash(self, id):
        self.netID = id
        userInfo = searchForNetID(self.netID)
        self.name = userInfo[1] + ' ' + userInfo[3]
        self.connections = searchForConnections(self.netID)
        self.rank = getUserRank(self.netID)
        self.suggestedFriends = findSuggestedFriends(id)
        self.missions = listCompletedMissions(id)

    def getNumConnections(self):
        return len(self.connections)

    def getNumSuggestedFriends(self):
        return len(self.suggestedFriends)

    def getNumMissions(self):
        numComplete = 0
        for mission in self.missions:
            if self.missions[mission]:
                numComplete = numComplete + 1
        return numComplete

# Flask ------------------------------------------------------------------------

# Index. Displays Login if not logged in. Displays dashboard if logged in/
@app.route('/')
def index():
    user = flask_login.current_user
    if user.is_authenticated:
        return redirect(url_for('dashboard'))
    else:
        return redirect(url_for('login'))

# Dashboard: Creates dashObj and passes it to dashboard
@app.route('/dashboard')
@flask_login.login_required
def dashboard():
    user = flask_login.current_user
    dash = dashObj()
    dash.loadDash(user.id)
    return render_template('dashboard.html', dashObj=dash)

# Connections: Displays List of all connections for user
@app.route('/connections')
def connections():
    user = flask_login.current_user
    netID = user.id
    cList = searchForConnections(netID)
    userInfo = searchForNetID(netID)
    name = userInfo[1] + ' ' + userInfo[3]
    return render_template('connections.html', cList=cList, userName=name)

# Login: Displays login fields and handles user login
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html', failedLogin=False)
    else:
        netID = request.form['login_id']
        password = request.form['pw']
        p0 = searchForNetID(netID)
        if isLoginValid(cursor, netID, password):
            user = User()
            user.id = netID
            flask_login.login_user(user)
            return redirect(url_for('dashboard'))
        else:
            return render_template('login.html', failedLogin=True)

# Logout: Log user out and redirect to login page
@app.route("/logout")
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return redirect(url_for('login'))

# New User: Show boxes to create user and handle user creation
@app.route('/newUser', methods=['GET', 'POST'])
def newUser():
    if request.method == 'GET':
        return render_template('newUser.html', failedCreate=False)
    else:
        netID = request.form['login_id']
        password = request.form['pw']
        password2 = request.form['pw2']
        if password != password2:
            return render_template('newUser.html', failedCreate=True)
        else:
            if createUser(password, netID):
                return redirect(url_for('login'))
            else:
                return render_template('newUser.html', failedCreate=True)

# New Connection: Handle creation of new conection
# Searches for user and displays information
# Verifies middle initial of searched person to verify human interaction
# Creates connection if everything checks out
@app.route("/newConnection", methods=['GET', 'POST'])
@flask_login.login_required
def newConnection():
    user = flask_login.current_user
    userInfo = searchForNetID(user.id)
    name = userInfo[1] + ' ' + userInfo[3]
    if request.method == 'GET':
        return render_template('newConnection.html', didCreate=False, didSearch=False, cUser=None, userName=name)
    else:
        # Search button pressed - Search for user and present information
        if request.form['submit'] == 'Search':
            netID = request.form['netID']
            user = searchForNetID(netID)
            # netID is valid
            if user:
                return render_template('newConnection.html', didCreate=False, didSearch=True, didConfirm=False, cUser=user, userName=name)
            # netID is invalid
            else:
                return render_template('newConnection.html', didCreate=False, didSearch=True, didConfirm=False, cUser=None, userNmae=name)

        # Create Button Pressed
        elif request.form['submit'] == 'Create':
            netID2 = request.form['netID']
            # Confirm Middle Initial
            middleName = searchForNetID(netID2)[2]
            if middleName == 'NULL':
                middleInitial = middleName[0]
            else:
                middleInitial = request.form['middleInitial']
            if middleInitial[0] != middleName[0] and middleInitial != 'Key':
                # No
                user = searchForNetID(netID2)
                return render_template('newConnection.html', didCreate=False, didSearch=True, didFailConfirm=True, cUser=user, userName=name)
            else:
                # Yes
                user = flask_login.current_user
                if createConnection(user.id, netID2):
                    return render_template('newConnection.html', didCreate=True, didSearch=False, didConfirm=False, cUser=None, userName=name)
                else:
                    return render_template('newConnection.html', didCreate=False, didSearch=False, didConfirm=False, cUser=None, alreadyExists=True, userName=name)
        else:
            print "ERROR"

# Missions: Display a list of all missions both complete and incomplete
@app.route('/missions', methods=['GET', 'POST'])
@flask_login.login_required
def missions():
    # Get User Information
    user = flask_login.current_user
    netID = user.id
    userInfo = searchForNetID(netID)
    name = userInfo[1] + ' ' + userInfo[3]
    # Get Mission Information
    missions_completed = listCompletedMissions(netID)
    mission_names = list()
    mission_success = list()
    mission_data = []
    for mission in missions_completed:
        if missions_completed[mission] == False:
            mission_names.append(mission)
            mission_success.append("Incomplete")
        else:
            mission_names.append(mission)
            mission_success.append("Complete")
    for i in range(len(mission_names)):
        mission_data.append((mission_names[i], mission_success[i]))
    #print mission_data
    return render_template('missions.html', missionData = mission_data, userName=name)

# Suggested Friends: Display a list of friends you should add
@app.route('/suggestedFriends', methods=['GET', 'POST'])
def suggestedFriends():
    #search for suggested friends and display the results
    user = flask_login.current_user
    netID = user.id
    cList = findSuggestedFriends(netID)
    userInfo = searchForNetID(netID)
    name = userInfo[1] + ' ' + userInfo[3]
    return render_template('suggestedFriends.html', cList=cList, userName=name)

# Leaderboard: Display a list of the top users of the game
@app.route('/leaderboard')
def leaderboard():
    user = flask_login.current_user
    netID = user.id
    userInfo = searchForNetID(netID)
    name = userInfo[1] + ' ' + userInfo[3]
    lb = buildLeaderboard()
    return render_template('leaderboard.html', lb=lb, userName=name)

# Connection Graph
@app.route('/connectionGraph')
def connectionGraph():
    user = flask_login.current_user
    netID = user.id
    userInfo = searchForNetID(netID)
    name = userInfo[1] + ' ' + userInfo[3]
    c = buildConGraph(netID)
    return render_template('connectionGraph.html',c=c, userName=name)

# Opt Out: Page to allow users to opt out of the game
@app.route('/optOut', methods=['GET', 'POST'])
def outOut():
    if request.method == 'GET':
        return render_template('optOut.html', failedOptOut=False, didOptOut=False)
    else:
        netID = request.form['login_id']
        middleName = request.form['middleName']
        user = searchForNetID(netID)
        if user[2][0] == middleName[0] or user[2] == 'NULL':
            optOutUser(cursor, netID)
            return render_template('optOut.html', failedOptOut=False, didOptOut=True)
        else:
            return render_template('optOut.html', failedOptOut=True, didOptOut=False)
